export const BeritaComponent = {
    data() {
        return {
            berita: [
                {
                    id: 1,
                    title: 'Berita Politik'
                },
                {
                    id: 2,
                    title: 'Berita Olahraga'
                },
                {
                    id: 3,
                    title: 'Berita Agama'
                },

            ]
        }
    },
    template: `
        <div>
            <h1>Daftar Berita</h1>
            <ul>
                <li v-for="detilBerita of berita">
                    <router-link :to="'/detilBerita/'+detilBerita.id"> 
                        {{ detilBerita.title }} 
                    </router-link>
                </li>
            </ul>
        </div>
    `
}