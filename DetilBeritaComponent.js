export const DetilBeritaComponent = {
    data() {
        return {
            detilBerita: [
                {
                    id: 1,
                    title: 'Berita Politik',
                    description: `Berita tentang segala hal di dunia politik negara indonesia`
                },
                {
                    id: 2,
                    title: 'Berita Olahraga',
                    description: `Berita tentang segala hal di dunia perolahragaan negara indonesia`

                },
                {
                    id: 3,
                    title: 'Berita Islam',
                    description: `Berita tentang perkembangan umat muslim di negara indonesia`

                },

            ]
        }
    },
    computed: {
        berita() {
            return this.detilBerita.filter((berita) => {
                return berita.id === parseInt(this.$route.params.id)
            })[0]
        }
    },
    template: `<div >
            <h1>Berita :  {{ berita.title }}</h1>
            <ul>
                <li v-for="(num, value) of berita">
                    {{ num +' : '+ value }} <br>
                </li>
            </ul>
        </div>`,

}